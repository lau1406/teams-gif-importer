# Teams Gif Importer

This is a simple script that can import Gifs into Microsoft Teams.
Currently only Windows 10/11 are supported.

## How to use

- Download `Import-Gifs.ps1` to a handy directory
- Put all your Gifs in the same directory as the `Import-Gifs.ps1` file
- Execute the `Import-Gifs.ps1` script

You might be asked if you trust this script and if you want to execute it

## What it does

The script first checks to see if you have [ImageMagick](https://imagemagick.org/index.php) installed. ImageMagick is used to extract a thumbnail from the Gif. This isn't strictly neccecary, but highly recommended. Without ImageMagick, the Gif is also used as a thumbnail, but with ImageMagick an actual image is extracted. The main advantage of doing this is to prevent slowdowns in Microsoft Teams. Microsoft Teams loads all thumbnails when you open the background selector. When these thumbnails are large files, this will slow down, in the worst case making it barely usable.

Next it loops over all Gifs in the directory. First it creates a thumbnail for the Gif and next it renames the Gif to `*.jpg` (this is what Microsoft Teams wants). Lastly it copies the two (2) `*.jpg` files to the correct directory where Microsoft Teams expects user backgrounds (`%appdata%\Microsoft\Teams\Backgrounds\Uploads`).
