# Version History

| Who         | When       | Version | What                                      |
|-------------|------------|---------|-------------------------------------------|
| L. Keijzer  | 2023-01-05 | 1.0     | Initial version                           |
| L. Keijzer  | 2023-01-05 | 1.1     | Wait for user interaction before exiting  |
| L. Keijzer  | 2023-01-19 | 1.2     | Add ASCII art + DRY up code               |
| L. Keijzer  | 2023-01-19 | 1.3     | Move version history to file              |
