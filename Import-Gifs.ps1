####################################################
#
# Author: Laurence Keijzer
# Note: Use on your own risk
#
####################################################

Write-Host @"
_______                          _____ _____ ______   _____                            _             
|__   __|                        / ____|_   _|  ____| |_   _|                          | |           
   | | ___  __ _ _ __ ___  ___  | |  __  | | | |__      | |  _ __ ___  _ __   ___  _ __| |_ ___ _ __ 
   | |/ _ \/ _` | '_ ` _ \/ __| | | |_ | | | |  __|     | | | '_ ` _ \| '_ \ / _ \| '__| __/ _ \ '__|
   | |  __/ (_| | | | | | \__ \ | |__| |_| |_| |       _| |_| | | | | | |_) | (_) | |  | ||  __/ |   
   |_|\___|\__,_|_| |_| |_|___/  \_____|_____|_|      |_____|_| |_| |_| .__/ \___/|_|   \__\___|_|   
                                                                      | |                            
                                                                      |_|                            
"@

Write-Host @"
#############################################################
#
# Author: Laurence Keijzer
# Verion: 1.3
# Note: Use on your own risk
# This will overwrite existing background files...
# Probably not a problem, 
# except when you have the same file name for diferrent gifs
#
#############################################################
"@

Read-Host "Press enter to continue, or ctrl + c to exit"

function Get-ImageMagick {  
    $potentialMagick = Get-ChildItem 'C:\Program Files\ImageMagick*\magick.exe'
    if ($potentialMagick.count -ge 1) {
        return $potentialMagick[0].fullName
    }
    return $false
}

# Add option to specify directory (default current dir)
$gifs = Get-ChildItem -Filter "*.gif"

$imageMagick = Get-ImageMagick
if (!$imageMagick) {
    Write-Host "It looks like you don't have Image Magick installed"
    Write-Host "This isn't a big problem, but it will result in larger thumbnail files, that might slow the Microsoft Teams background selection window down."
    $install = Read-Host "Would you like to install this? Y or N"
    While (($install -ne "Y") -and ($install -ne "N")) {
        $install = Read-Host "Please select either 'Y' or 'N'"
    }
    if ($install -eq "Y") {
        winget install imagemagick
        $imageMagick = Get-ImageMagick
    }
}

$teamsImageDir = "$env:appdata\Microsoft\Teams\Backgrounds\Uploads"
Write-Host "Getting all gifs"
foreach ($gif in $gifs) {
    $gifFullName = $gif.fullName
    $gifDirName = $gif.directoryName
    $gifName = [System.IO.Path]::GetFileNameWithoutExtension($gif)
    $gifNameThumb = "$($gifName)_thumb.jpg"
    $gifFullNameThumb = "$gifDirName\$gifNameThumb"
    $gifNameJpg = "$gifName.jpg"
    $gifFullnameJpg = "$gifDirName\$gifNameJpg"

    Write-Host "Importing $gifName"
    if ($imageMagick) {
        # If has image magick: get small jpg
        & $imageMagick "$gifFullName[0]" "$gifFullNameThumb"
    } else {
        # Else, copy and rename with _thumb
        Copy-Item "$gifFullName" "$gifFullNameThumb"
    }
    # Rename to .jpg
    Move-Item "$gifFullName" "$gifFullnameJpg"

    # Move to correct dir
    Copy-Item "$gifFullnameJpg" "$teamsImageDir"
    Copy-Item "$gifFullNameThumb" "$teamsImageDir"

    Write-Host "Done processing $gifName"
}

Write-Host "Done processing, enjoy your backgrounds =]"
Read-Host "Press enter to exit"
exit 0
